'use strict';
const createBooksTable = require('./tables/books');

const createTables = ((database) => {
    createBooksTable.createBooksTable(database);
});

module.exports = {
    createTables: createTables,
};