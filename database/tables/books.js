const createBooksTable = ((database) => {
    database.schema.hasTable('books').then(function (exists) {
        if (!exists) {
            return database
                .schema
                .createTable('books', function (table) {
                    table.increments('id').primary();
                    table.string('title');
                    table.string('author');
                    table.string('publication_year');
                    table.timestamps();
                }).then(console.log('Created new table "books"'));
        } else {
            return console.log('Table "books" already exist')
        }
    });
});

module.exports = {
    createBooksTable: createBooksTable,
};