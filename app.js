const express = require('express');
const bodyParser = require('body-parser');

const knex = require('knex');
const book = require('./controllers/book');
const {createTables} = require('./database/setUp');

const app = express();
app.use(bodyParser.json());

const database = knex({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        user: 'postgres',
        password: 'motylek1',
        database: 'smart-brain'
    }
});

createTables(database);

app.get('/:title', (request, response) => {
    book.handleGetBooks(request, response);
});

app.post('/book/save', (request, response) => {
    console.log(`app.js + ${request.body}`);
    book.handleSaveBook(request, response, database);
});

app.listen(8080, () => {
    console.log('App is running on port 8080')
});
