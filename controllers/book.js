const fetch = require('node-fetch');

const handleGetBooks = (request, response) => {

    const {title} = request.params;
    console.log(`http://data.bn.org.pl/api/bibs.json?title=${title}`);
    fetch(`http://data.bn.org.pl/api/bibs.json?title=${title}`)
        .then((resp) => resp.json())
        .then(data => response.json(data))
        .catch(error => response.status(400).json('unable to work with api' + error))
};

const handleSaveBook = (request, response, database) => {
        console.log(`Save book + ${request.body.title}`);
        const {title, author, publicationYear} = request.body;
        database.transaction(transaction => {
            transaction.insert({
                title: title,
                author: author,
                publication_year: publicationYear,
            }).into('books')
                .then(transaction.commit)
                .then(response.json('Completed'))
                .catch(transaction.rollback);
        })

    }
;

module.exports = {
    handleGetBooks, handleSaveBook
};